Примеры CI-CD
=============


| Назначение скрипта                                      | Ссылка                                                    |
| :--                                                     | :--                                                       |
| Встроенный валидатор yaml файдлов                       | [lint](https://gitlab.com/dm_2008/dm-system-up/-/ci/lint) |
| **Установка сред**                                      |                                                           |
| Установка php 7.2 на Ubuntu 18.04                       | [php](./sys/ubuntu-php.sh)                                |
| Установка Postgres 10.2 на Ubuntu 18.04                 | [postgres](./sys/ubuntu-postgres10.sh)                    |
| Создание БД в Postgres 10.2                             | [new database](./sys/ubuntu-postgres10-db.sh)             |
| Установка редактора VIM                                 | [VIM](./sys/ubuntu-vim.sh)                                |
| **Настройка соединения с удаленным сервером**           |                                                           |
| Пример подключения к серверу через sshpass              | [ssh-bypass](./ssh-up/01-ssh-bypass.sh)                   |
| Обнуление known_hosts                                   | [clean-knownhosts](./ssh-up/02-clean-known-hosts.sh)      |
| Создание ключей для обмена с удаленным сервером         | [generate-keys](./ssh-up/03-generate-keys.sh)             |
| Добавление секретного ключа на локальную машину         | [add-local-keys](./ssh-up/04-add-local-key.sh)            |
| Добавление публичного ключа на удаленный сервер         | [generate-keys](./ssh-up/05-copy-remote-key.sh)           |
| Подключение с помощью с использованием ключа            | [ssh-bykey](./ssh-up/06-ssh-bykey.sh)                     |
| **Примеры конфигурационных файлов CI-CD**               |                                                           |
| Несколько примеров собранных в одном файле              | [All in One File](./yml-example/allinone.gitlab-ci.yml)   |
| Пример задания порядка при выполнении задач             | [Jobs order](./yml-example/jobsorder.gitlab-ci.yml)       |
| Доступ к удаленному серверу через SSH и пароль          | [SSH by Key](./yml-example/key.gitlab-ci.yml)             |
| Доступ к удаленному серверу через SSH и ключ шифрования | [SSH by Password](./yml-example/sshpass.gitlab-ci.yml)    |
| Пример группировки задач в один пакет                   | [Linked Jobs](./yml-example/linkedjobs.gitlab-ci.yml)     |
| **Ссылки на справку gitlab yml** |                                                                               |
| Деление сценария на файлы        | https://docs.gitlab.com/ee/ci/yaml/README.html#include                        |
| Код общий для всех задач         | https://docs.gitlab.com/ee/ci/yaml/README.html#before_script-and-after_script |
| Триггеры                         | https://docs.gitlab.com/ee/ci/yaml/README.html#trigger                        |
|                                  | https://docs.gitlab.com/ee/ci/triggers/README.html                            |
| Консоль управления - чат         | https://about.gitlab.com/handbook/communication/chat/                         |
| Монитор процессов                | https://docs.gitlab.com/ee/ci/metrics_reports.html                            |
| Web API                          | https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#webhooks   |

# Определение конвееров Git Lab

## Сценарий пост обработки кода в репозитории
Для репозитория `gitlab` существует возможность задать выполнение некоторых
команд после того, как в репозиторий попадает новый код.

Сценарий описывается в файле формата yml. Имя головного файла сценария задается
в настройках репо. По умолчанию это `.gitlab-ci.yml`.

Некоторые служебные слова:

| Термин              | Назначение                                                                                      |
| :--                 | :--                                                                                             |
| image               | Docker образ, может быть задан как для сценария целиком, так и для отдельной задачи             |
| script              | Начало секции с командами, bash, ruby и т.д в зависимости от выбранного образа                  |
| stages              | Перечень этапов и порядок их запуска                                                            |
| stage               | Назначение этапа для задачи                                                                     |
| include             | Ссылка на внешний файл сценария, итоговый файл сценария будет как результат склейки этих файлов |
| - local             | Для секции `include`, задает поиск файла в локальном репо                                       |
| allow_failure: true | Не останавливает конвеер при подении задачи с таким заданным свойством                          |
| when: manual        | Такая задача запускается только в ручную из Web-интерфейса                                      |
| - echo              | Пример bash команды `echo` в секции script                                                      |

## Структура файла сценария

```
<имя задачи>:
    # Коментарий
    <Ключ>: <Значение>
    script:
        - <bash команда>
```

* Пример:
```
run-test:
  script:
    - ruby --version
```

## image: Окружение задачи
Для задачи можно выбрать различные docker образы - окружение, в котором будет выполнятся задача.
Некоторые допустимые значения:
* ubuntu
* ubuntu:19.10
* ubuntu:18.04
* debian
* ruby
* golang

* Пример:
```
job 1:
    image: ubuntu:19.10
    script:
        - echo "script run"
```

## stages: Порядок выполнения скриптов

Каждой задаче можно присвоить тэг `stage`, и задать порядок выполнения этих этапов.
Задать можно еще много чего, например ручной запуск этапа `when: manual`.

Для отладки yaml скрипта можно воспользоватся встроенной [ссылкой](https://gitlab.com/dm_2008/dm-system-up/-/ci/lint)

Пример:
```
stages:
- ontest
- test
- onprod

job 1:
    stage: test
    script:
    - echo "Job 2"

job 2:
    stage: ontest
    script:
    - echo "Job 1 - Deploy on Test"

job 3:
    stage: onprod
    when: manual
    script:
    - echo "Job 3 - !!! Deploy on Prod..."
```

# Развертывание среды на удаленном сервере

## Удаленный запуск команды
* Запуск команды uptime
```
ssh root@127.0.0.1 uptame
```

## Удаленный запуск сценария
* Запуск файла init.sh
```
ssh root@127.0.0.1 'bash -s' < init.sh
```

## Настройка удаленного соединения по SSH (с использованием ключа для шифрования)

* Создать ключ, [скрипт](./ssh-up/03-generate-keys.sh)
* Скопировать публичный ключ на удаленный сервер, [скрипт](./ssh-up/05-copy-remote-key.sh)
* Добавить секретный ключ на локальный узел, [скрипт](./ssh-up/04-add-local-key.sh)
* Пример сценария: [SSH by Key](./yml-example/key.gitlab-ci.yml)           |

### Пример:

* Настройка
```
ssh-keygen  -t ecdsa -b 521 -f deploy_key -C "dm_2008@mail.ru" -P ""

sshpass -p StrongPa$w0rd ssh-copy-id -o StrictHostKeyChecking=no -i ./deploy_key.pub -p 22 root@23.44.0.1

ssh-add ./deploy_key
ssh-keyscan -t ecdsa $host >> ~/.ssh/known_hosts
```

* Подключение
```
ssh -p $port -o StrictHostKeyChecking=no root@$23.44.0.1
```

## Настройка удаленного соединения по SSH (с использованием sshpass)
* Пример сценария: [SSH by Password](./yml-example/sshpass.gitlab-ci.yml)

### Пример:
```
sshpass -p StrongPa$w0rd ssh -p 22 -o StrictHostKeyChecking=no root@$23.44.0.1
```

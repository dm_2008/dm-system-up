#!/usr/bin/env bash
echo "Install VIM"
apt-get update
apt-get vim

vim --version | head -n 1

echo "Done."

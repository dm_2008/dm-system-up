#!/usr/bin/env bash
# root соединение с виртуальной машиной
reset
host=$(<../ssh-up/param_host)
port=$(<../ssh-up/param_port)
key=$(<../ssh-up/param_key)
echo "Подключение к серверу: $host:$port"
sshpass -p $key ssh -p $port root@$host
#exo-open "vagrant ssh -c 'sudo bash'" --launch TerminalEmulator

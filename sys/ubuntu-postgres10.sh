#!/usr/bin/env bash
echo "Install Postgres 10.12"

# Зависимости
apt update
apt install -y sudo

# Установка СУБД
DEBIAN_FRONTEND=noninteractive apt install -y postgresql postgresql-contrib

# Пароль для postgres пользователя
sudo -u postgres psql -c "alter user postgres with password 'StrongAdminP@ssw0rd'"

# Внешний доступ
ss -nlt | grep 5432
sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'         /g" /etc/postgresql/10/main/postgresql.conf 

echo ""
echo "Restart postgres service."
service postgresql restart

# Отчет о прослушивании портов
ss -nlt | grep 5432

# Отчет о состоянии сервиса СУБД
systemctl status postgresql.service

# Отчет о доступных БД
sudo -u postgres psql -l

# Отчет о версии клиента
psql --version
echo "Done."

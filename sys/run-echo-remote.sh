#!/usr/bin/env bash
reset
host=$(<../ssh-up/param_host)
port=$(<../ssh-up/param_port)
key=$(<../ssh-up/param_key)
login="root"
echo "Запуск скрипта ubuntu-echo на сервере: $host:$port"
echo Host: $host
echo Port: $port
echo Key: $key
echo Login: $login
sshpass -p $key ssh -p $port $login@$host "bash -s" < "ubuntu-echo.sh"

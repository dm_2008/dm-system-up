#!/usr/bin/env bash
# Копирование ключей на удаленный сервер
host=$(<param_host)
port=$(<param_port)

# Удаление старого ключа
# ssh-keygen -f "/home/cat/.ssh/known_hosts" -R $host > /dev/null
# ssh-keygen -f "/home/cat/.ssh/known_hosts" -R [$host] > /dev/null
# ssh-keygen -f "/home/cat/.ssh/known_hosts" -R [$host]:$port > /dev/null

# Установка правильных разрешений
#chmod 400 ./deploy_key

# Добавление ключа
ssh-add ./deploy_key
ssh-keyscan -t ecdsa $host >> ~/.ssh/known_hosts

#!/usr/bin/env bash
# Копирование ключей на удаленный сервер
host=$(<param_host)
port=$(<param_port)
key=$(<param_key)
login="root"
sshpass -p $key ssh-copy-id -o StrictHostKeyChecking=no -i ./deploy_key.pub -p $port $login@$host

#!/usr/bin/env bash
# Удаление локальных ключей
local_user=$(whoami)
n=2
reset
echo "Внимание !!! "
echo "Удаление всех сохраненных ключей ($n сек)"
echo "Файл [/home/$local_user/.ssh/known_hosts] будет удален."

for s in $(seq $n)
do
    sleep 1
    printf ". "
done
sleep 1
printf "\n"

rm -f "/home/$local_user/.ssh/known_hosts"

echo ""
echo "Файл [/home/$local_user/.ssh/known_hosts] удален."


# ssh-keygen -f "/home/cat/.ssh/known_hosts" -R $host > /dev/null
# ssh-keygen -f "/home/cat/.ssh/known_hosts" -R [$host] > /dev/null
# ssh-keygen -f "/home/cat/.ssh/known_hosts" -R [$host]:$port > /dev/null

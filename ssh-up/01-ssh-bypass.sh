#!/usr/bin/env bash
# Копирование ключей на удаленный сервер
host=$(<param_host)
key=$(<param_key)
port=$(<param_port)
local_user=$(whoami)

sshpass -p $key ssh -p $port -o StrictHostKeyChecking=no root@$host
